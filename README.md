# Revolut interview task
Application provides simplified REST API to perform money transfer between accounts. 

For the sake of simplicity it does not use any DI framework and have in-memory db based on `ConcurrentHashMap`. 

It is absolutely minimalistic( eg. it uses default exception handler and logging is limited)

### Stack
* Kotlin
* Gradle
* Ratpack

### Building & Running
#####Building
`./gradlew build`

#####Running

Application by default starts on `8080` port

` java -jar build/libs/revolut-interview-1.0-SNAPSHOT.jar`

### API

####Create account
    POST /account

|Name|Type|Description|
|----|----|-----------|
|`id`|`integer`| account id|
|`balance`|`decimal`|account balance|
    
    
####Modifying account
    PUT /account

|Name|Type|Description|
|----|----|-----------|
|`id`|`integer`| account id which will be modified|
|`balance`|`decimal`|new account balance|


####Getting account
    GET /account/:id
    
    
####Transfer money between accounts
    POST /account/transfer

|Name|Type|Description|
|----|----|-----------|
|`senderAccountId`|`integer`| sender account id|
|`receiverAccountId`|`integer`| receiver account id|
|`amount`|`decimal`|amount of money to be send|