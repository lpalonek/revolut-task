package pl.palonek.revolut.repository

import org.slf4j.LoggerFactory
import pl.palonek.revolut.exceptions.AccountAlreadyExistException
import pl.palonek.revolut.exceptions.AccountNotFoundException
import pl.palonek.revolut.model.Account
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

class InMemoryAccountRepository {

    companion object {
        val log = LoggerFactory.getLogger(InMemoryAccountRepository::class.java)
    }
    private val accountStore = ConcurrentHashMap<Int, Account>()
    private val lockMap = ConcurrentHashMap<Int, Lock>()

    fun createAccount(account: Account) = lock(account) {
        val prevValue = accountStore.putIfAbsent(account.accountNumber, account)
        if (prevValue != null) {
            log.error("Account with id: ${account.accountNumber} already exists")
            throw AccountAlreadyExistException("Account with id: ${account.accountNumber} already exists")
        }
    }

    fun fetchAccount(id: Int): Account {
        if (accountStore.containsKey(id)) {
            return accountStore[id]!!
        } else {
            log.error("Account with id: $id does not exist")
            throw AccountNotFoundException("Account with id: $id does not exist")
        }
    }

    fun modifyAccount(modifiedAccount: Account) = lock(modifiedAccount) {
        val accountNumber = modifiedAccount.accountNumber
        if (accountStore.containsKey(accountNumber)) {
            accountStore[accountNumber] = modifiedAccount
        } else {
            log.error("Account with id: $accountNumber does not exist")
            throw AccountNotFoundException("Account with id: $accountNumber does not exist")
        }
    }

    fun lockKey(id: Int) {
        lockMap.getOrPut(id) { ReentrantLock() }.lock()
    }

    fun unlockKey(id: Int) {
        lockMap.getOrPut(id) { ReentrantLock() }.unlock()
    }

    private inline fun lock(account: Account, block: (Account) -> Unit) {
        val lock = lockMap.getOrPut(account.accountNumber) { ReentrantLock() }
        lock.lock()
        try {
            block(account)
        } finally {
            lock.unlock()
        }
    }
}