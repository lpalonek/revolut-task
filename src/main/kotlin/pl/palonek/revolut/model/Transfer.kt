package pl.palonek.revolut.model

import java.math.BigDecimal

data class Transfer(
    val senderAccountId: Int,
    val receiverAccountId: Int,
    val amount: BigDecimal
) {

    constructor() : this(0, 0, BigDecimal.ZERO)
}
