package pl.palonek.revolut.model

import java.math.BigDecimal

data class Account(
    val accountNumber: Int = 0,
    val balance: BigDecimal = BigDecimal.ZERO
) {
    constructor() : this(0, BigDecimal.ZERO)
}