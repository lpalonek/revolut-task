package pl.palonek.revolut

import pl.palonek.revolut.model.Account
import pl.palonek.revolut.model.Transfer
import pl.palonek.revolut.repository.InMemoryAccountRepository
import pl.palonek.revolut.service.AccountService
import pl.palonek.revolut.service.TransferService
import ratpack.handling.Context
import ratpack.jackson.Jackson.json
import ratpack.server.RatpackServer
import ratpack.server.ServerConfig
import java.net.URI

object Application {

    @JvmStatic
    fun main(args: Array<String>) {

        val repos = InMemoryAccountRepository()
        val accountService = AccountService(repos)
        val transferService = TransferService(repos)
        RatpackServer.start { server ->
            server.serverConfig(ServerConfig.embedded().publicAddress(URI("localhost")).port(8080))
                .handlers { chain ->
                    chain.prefix("account") { accountChain ->
                        accountChain
                            .path { ctx ->
                                ctx.byMethod { spec ->
                                    spec.put { ctx -> modifyAccountHandler(ctx, accountService) }
                                    spec.post { ctx -> createAccountHandler(ctx, accountService) }
                                }
                            }
                            .path("transfer") { ctx ->
                                ctx.byMethod { spec ->
                                    spec.post { ctx -> transferHandler(ctx, transferService) }
                                }
                            }
                            .path(":id") { ctx ->
                                ctx.byMethod { spec ->
                                    spec.get { ctx -> fetchAccountHandler(ctx, accountService) }
                                }
                            }

                    }
                }
        }
    }

    private fun fetchAccountHandler(context: Context, accountService: AccountService) {
        context.render(
            json(
                accountService.fetchAccount(context.pathTokens["id"]!!.toInt())
            )
        )
    }

    private fun createAccountHandler(context: Context, accountService: AccountService) {
        context.parse(Account::class.java).then { account ->
            accountService.createAccount(account)
            context.render(json(account))
        }
    }

    private fun modifyAccountHandler(context: Context, accountService: AccountService) {
        context.parse(Account::class.java).then { account ->
            accountService.modifyACcount(account)
            context.render(json(account))
        }
    }

    private fun transferHandler(context: Context, transferService: TransferService) {
        context.parse(Transfer::class.java)
            .then { transfer ->
                transferService.transferMoney(transfer)
                context.render("ok")
            }
    }
}



