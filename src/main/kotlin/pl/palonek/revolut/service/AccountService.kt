package pl.palonek.revolut.service

import pl.palonek.revolut.model.Account
import pl.palonek.revolut.model.Transfer
import pl.palonek.revolut.repository.InMemoryAccountRepository

class AccountService(private val accountRepository: InMemoryAccountRepository) {
    fun createAccount(account :Account){
        accountRepository.createAccount(account)
    }

    fun modifyACcount(account:Account){
        accountRepository.modifyAccount(account)
    }

    fun fetchAccount(id:Int): Account {
        return accountRepository.fetchAccount(id)
    }
}