package pl.palonek.revolut.service

import org.slf4j.LoggerFactory
import pl.palonek.revolut.exceptions.MoneyTransferException
import pl.palonek.revolut.model.Account
import pl.palonek.revolut.model.Transfer
import pl.palonek.revolut.repository.InMemoryAccountRepository
import java.math.BigDecimal

class TransferService(private val accountRepository: InMemoryAccountRepository) {

    companion object {
        private val log = LoggerFactory.getLogger(TransferService::class.java)
    }

    fun transferMoney(transfer: Transfer) {
        val receiverId = transfer.receiverAccountId
        val senderId = transfer.senderAccountId
        validateAccountIds(receiverId, senderId)
        validateTransferBalance(transfer)

        try {
            lockAccounts(receiverId, senderId)

            val receiver = accountRepository.fetchAccount(receiverId)
            val sender = accountRepository.fetchAccount(senderId)
            validateAvailableFunds(sender, transfer)

            val updatedReceiver = receiver.copy(balance = receiver.balance + transfer.amount)
            val updatedSender = sender.copy(balance = sender.balance - transfer.amount)

            accountRepository.modifyAccount(updatedReceiver)
            accountRepository.modifyAccount(updatedSender)

        } finally {
            unlockAccounts(receiverId, senderId)
        }
    }

    private fun validateAvailableFunds(sender: Account, transfer: Transfer) {
        if (sender.balance < transfer.amount) {
            log.error("Insufficient money")
            throw MoneyTransferException("Insufficient money")
        }
    }

    private fun validateTransferBalance(transfer: Transfer) {
        if (transfer.amount <= BigDecimal.ZERO) {
            log.error("Amount of money cannot be zero or negative")
            throw MoneyTransferException("Amount of money cannot be zero or negative")
        }
    }

    private fun validateAccountIds(receiverId: Int, senderId: Int) {
        if (receiverId == senderId) {
            log.error("Cannot transfer money to the same account")
            throw MoneyTransferException("Cannot transfer money to the same account")
        }
    }

    private fun unlockAccounts(receiverId: Int, senderId: Int) {
        if (receiverId < senderId) {
            accountRepository.unlockKey(receiverId)
            accountRepository.unlockKey(senderId)
        } else {
            accountRepository.unlockKey(senderId)
            accountRepository.unlockKey(receiverId)
        }
    }

    private fun lockAccounts(receiverId: Int, senderId: Int) {
        if (receiverId < senderId) {
            accountRepository.lockKey(receiverId)
            accountRepository.lockKey(senderId)
        } else {
            accountRepository.lockKey(senderId)
            accountRepository.lockKey(receiverId)
        }
    }
}