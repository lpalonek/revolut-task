package pl.palonek.revolut.exceptions

class MoneyTransferException(message: String?) : Exception(message)