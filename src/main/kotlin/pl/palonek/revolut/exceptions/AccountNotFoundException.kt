package pl.palonek.revolut.exceptions

class AccountNotFoundException(message: String) : Exception(message)