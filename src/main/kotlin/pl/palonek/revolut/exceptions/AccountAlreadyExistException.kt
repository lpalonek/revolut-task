package pl.palonek.revolut.exceptions

class AccountAlreadyExistException(message: String?) : Exception(message)