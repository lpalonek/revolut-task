package pl.palonek.revolut.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import pl.palonek.revolut.exceptions.AccountAlreadyExistException
import pl.palonek.revolut.exceptions.AccountNotFoundException
import pl.palonek.revolut.model.Account
import pl.palonek.revolut.repository.InMemoryAccountRepository

internal class AccountServiceTest {

    private val accountService = AccountService(InMemoryAccountRepository())


    @Test
    fun `should create account`() {
        //given
        val expected = Account(1, 20.toBigDecimal())

        //when
        accountService.createAccount(expected)
        val actual = accountService.fetchAccount(1)

        //then
        assertEquals(expected, actual)
    }

    @Test
    fun `should throw exception when account to be created already exist`() {
        //given
        val account = Account(1, 20.toBigDecimal())

        //when
        accountService.createAccount(account)

        //then
        assertThrows<AccountAlreadyExistException> { accountService.createAccount(account) }
    }

    @Test
    fun `should throw exception when account does not exist`() {
        //given
        val accountToModify = Account(5, 3.toBigDecimal())
        val notExistingAccountId = 4

        //when
        assertThrows<AccountNotFoundException> { accountService.fetchAccount(notExistingAccountId) }
        assertThrows<AccountNotFoundException> { accountService.modifyACcount(accountToModify) }
    }
}