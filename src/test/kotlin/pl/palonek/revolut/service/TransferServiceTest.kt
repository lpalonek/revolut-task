package pl.palonek.revolut.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTimeout
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import pl.palonek.revolut.exceptions.MoneyTransferException
import pl.palonek.revolut.model.Account
import pl.palonek.revolut.model.Transfer
import pl.palonek.revolut.repository.InMemoryAccountRepository
import java.math.BigDecimal
import java.time.Duration
import java.util.concurrent.*

internal class TransferServiceTest {

    private lateinit var accountService: AccountService

    private lateinit var transferService: TransferService

    @BeforeEach
    internal fun setUp() {
        val accountRepository = InMemoryAccountRepository()
        accountService = AccountService(accountRepository)
        transferService = TransferService(accountRepository)
    }

    @Test
    fun `should transfer money between two accounts with multiple threads`() {
        //given
        val senderAccount = Account(1, 100000.toBigDecimal())
        val receiverAccount = Account(2, 0.toBigDecimal())
        val numberOfThreads = 500
        val threadPool = Executors.newFixedThreadPool(numberOfThreads)
        accountService.createAccount(senderAccount)
        accountService.createAccount(receiverAccount)
        val barrier = CyclicBarrier(numberOfThreads)
        val countDownLatch = CountDownLatch(numberOfThreads)

        val queue = ConcurrentLinkedQueue<BigDecimal>()
        //when
        for (i in 1..numberOfThreads) {
            threadPool.execute {
                barrier.await()
                val randomAmount = ThreadLocalRandom.current().nextInt(1, 20).toBigDecimal()
                transferService.transferMoney(Transfer(1, 2, randomAmount))
                queue.add(randomAmount)
                countDownLatch.countDown()
            }
        }
        threadPool.shutdown()

        countDownLatch.await()
        //then
        val actualBalance = accountService.fetchAccount(2).balance
        val expectedBalance = queue.fold(BigDecimal.ZERO, BigDecimal::add)
        assertEquals(expectedBalance, actualBalance)
    }

    @Test
    fun `should transfer money from multiple(200) accounts to multiple(100) accounts via proxy account in the middle, everything with multithreaded way`() {
        //given
        val numberOfThreads = 300
        val threadPool = Executors.newFixedThreadPool(numberOfThreads)
        for (i in 1..300) {
            if (i < 201) {
                accountService.createAccount(Account(i, BigDecimal(20)))
            } else {
                accountService.createAccount(Account(i, 0.toBigDecimal()))
            }
        }
        //create proxy account
        accountService.createAccount(Account(0, 4000.toBigDecimal()))

        val barrier = CyclicBarrier(numberOfThreads)
        val countDownLatch = CountDownLatch(numberOfThreads)

        //when
        for (i in 1..numberOfThreads) {
            threadPool.execute {
                barrier.await()
                if (i < 201) {
                    transferService.transferMoney(Transfer(i, 0, 20.toBigDecimal()))
                } else {
                    transferService.transferMoney(Transfer(0, i, 40.toBigDecimal()))
                }
                countDownLatch.countDown()
            }
        }
        threadPool.shutdown()
        countDownLatch.await()

        //then
        val actualBalance = accountService.fetchAccount(0).balance
        val expectedProxyBalanceBalance = BigDecimal(4000)
        assertEquals(expectedProxyBalanceBalance, actualBalance)

        val totalBalanceOfOutboundAccounts =
            (1..200).map { accountService.fetchAccount(it).balance }.fold(BigDecimal.ZERO, BigDecimal::add)
        assertEquals(BigDecimal.ZERO, totalBalanceOfOutboundAccounts)
        val totalBalanceOfInboundAccounts =
            (201..300).map { accountService.fetchAccount(it).balance }.fold(BigDecimal.ZERO, BigDecimal::add)
        assertEquals(BigDecimal(4000), totalBalanceOfInboundAccounts)
    }

    @Test
    fun `should randomly transfer money between accounts and make sure that total money in the whole system stays the same`() {
        assertTimeout(Duration.ofSeconds(10)) {
            val numberOfThreads = 500
            val threadPool = Executors.newFixedThreadPool(numberOfThreads)
            val numberOfAccounts = 2
            for (i in 1..numberOfAccounts) {
                accountService.createAccount(Account(i, 10000.toBigDecimal()))
            }

            val barrier = CyclicBarrier(numberOfThreads)
            val countDownLatch = CountDownLatch(numberOfThreads)

            //when
            repeat(numberOfThreads) {
                threadPool.execute {
                    barrier.await()
                    val amount = ThreadLocalRandom.current().nextInt(1, 20).toBigDecimal()
                    val randomPair = (1..numberOfAccounts).shuffled().subList(0, 2)
                    val receiverId = randomPair[0]
                    val senderId = randomPair[1]
                    transferService.transferMoney(Transfer(senderId, receiverId, amount))
                    countDownLatch.countDown()
                }
            }
            threadPool.shutdown()
            countDownLatch.await()

            //then
            val totalBalanceOfOutboundAccounts =
                (1..numberOfAccounts).map { accountService.fetchAccount(it).balance }
                    .fold(BigDecimal.ZERO, BigDecimal::add)
            assertEquals(BigDecimal(10000 * numberOfAccounts), totalBalanceOfOutboundAccounts)
        }
    }

    @Test
    fun `should throw exception when amount of money is not positive`() {
        //given
        val transfer = Transfer(1, 2, (-5).toBigDecimal())
        //when

        assertThrows<MoneyTransferException> { transferService.transferMoney(transfer) }
    }

    @Test
    fun `should throw exception when account is the same`() {
        //given
        val sender = Account(1, 50.toBigDecimal())
        val receiver = Account(2, 50.toBigDecimal())
        accountService.createAccount(sender)
        accountService.createAccount(receiver)
        val transfer = Transfer(1, 1, 5.toBigDecimal())
        //when

        assertThrows<MoneyTransferException> { transferService.transferMoney(transfer) }
    }

    @Test
    fun `should throw exception when account does not have enough money to transfer`() {
        //given
        val sender = Account(1, 50.toBigDecimal())
        val receiver = Account(2, 50.toBigDecimal())
        accountService.createAccount(sender)
        accountService.createAccount(receiver)
        val transfer = Transfer(1, 2, 105.toBigDecimal())
        //when

        assertThrows<MoneyTransferException> { transferService.transferMoney(transfer) }
    }
}