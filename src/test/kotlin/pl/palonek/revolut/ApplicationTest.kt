package pl.palonek.revolut

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import ratpack.http.Status
import ratpack.http.client.ReceivedResponse
import ratpack.test.MainClassApplicationUnderTest

class ApplicationTest {
    var appUnderTest = MainClassApplicationUnderTest(Application::class.java)

    @Test
    fun `create account endpoint test`() {
        //given
        val account = "{\"accountNumber\":1,\"balance\":5}"

        //when
        val accountCreateResult = createAccount(account)

        //then
        assertEquals(Status.OK, accountCreateResult.status)
        assertEquals(account, accountCreateResult.body.text)
    }

    @Test
    fun `should fetch created account`() {
        //given
        val account = "{\"accountNumber\":1,\"balance\":5}"
        createAccount(account)

        //when
        val response = appUnderTest.httpClient.get("account/1")

        //then
        assertEquals(Status.OK, response.status)
        assertEquals(account, response.body.text)
    }

    @Test
    fun `should modify created account`() {
        //given
        val oldAccount = "{\"accountNumber\":1,\"balance\":5}"
        val newAccount = "{\"accountNumber\":1,\"balance\":50}"

        createAccount(oldAccount)

        //when
        val response = appUnderTest.httpClient.requestSpec { request ->
            request.body {
                it.type("application/json")
                    .text(newAccount)
            }
        }.put("account")

        //then
        assertEquals(Status.OK, response.status)
        assertEquals(newAccount, response.body.text)
    }

    @Test
    fun `should transfer money from one account to another`() {
        //given
        val sender = "{\"accountNumber\":1,\"balance\":50}"
        val receiver = "{\"accountNumber\":2,\"balance\":50}"
        val expectedResult = "{\"accountNumber\":2,\"balance\":100}"
        createAccount(sender)
        createAccount(receiver)
        val transfer = "{\"senderAccountId\":1, \"receiverAccountId\":2, \"amount\":50}"

        //when
        val transferResponse = appUnderTest.httpClient.requestSpec { request ->
            request.body {
                it.type("application/json")
                    .text(transfer)
            }
        }.post("account/transfer")

        val account = appUnderTest.httpClient.get("account/2")

        //then
        assertEquals(Status.OK, transferResponse.status)
        assertEquals(expectedResult, account.body.getText())
    }

    private fun createAccount(receiver: String): ReceivedResponse {
        return appUnderTest.httpClient.requestSpec { request ->
            request.body {
                it.type("application/json")
                    .text(receiver)
            }
        }.post("account")
    }
}